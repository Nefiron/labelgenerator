<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once 'lib/CurlWrapper.php';

class LabelGenerator extends Module
{
    // Module Class Constructor
    public function __construct()
    {
        $this->name = 'labelgenerator';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'Alexander Doerge';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Shopivers Label Generator');
        $this->description = $this->l('Label notifier and generator for Shopivers Prestashops.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('LABEL_GENERATOR')) {
            $this->warning = $this->l('No name provided');
        }
    }

    // Module Install Hook
    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        
        if (!parent::install() ||
            !$this->registerHook('actionOrderStatusPostUpdate') ||
            !$this->registerHook('header') ||
            !Configuration::updateValue('LABEL_GENERATOR', 'Label Generator')
        ) {
            return false;
        }
        
        return true;
    }

    // Module Uninstall Hook
    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('LABEL_GENERATOR')
        ) {
            return false;
        }
    
        return true;
    }

    // Hook for status on Order
    public function hookActionOrderStatusPostUpdate(array $params)
    {

        if ($params['newOrderStatus']->name == 'Shipped')
        {
            // Initialize Order Object
            $order = new Order((int)$params['id_order']);

            // Initialize Customer Object
            $customer = new Customer((int)$order->id_customer);

            // Initialize Delivery Address Object
            $address = new Address((int)$order->id_address_delivery);

            // Initialize Curl Lib
            try {
                $curl = new CurlWrapper();
            } catch(CurlWrapperException $e) {
                echo $e->getMessage();
            }
            
            // Set headers and get API Key
            $curl->addHeader('Content-Type', 'application/json');
            $curl->addHeader('Accept', 'application/json');
	        $curl->addHeader('Authorization', 'Basic ' . Configuration::get('LABEL_APIKEY'));

            // JSON Encode Shipment
            $shipment = json_encode($this->createShipment($order, $customer, $address));

            // Send POST Request to Pakkelabels.dk API
            $shipment_response = $curl->rawPost('https://app.pakkelabels.dk/api/public/v3/shipments', $shipment);

            // Decode JSON Response from Pakkelabels.dk API
            $shipment_response_decoded = json_decode($shipment_response, true);

            // Decode Label PDF. (base64)
            $pdf_decoded = base64_decode($shipment_response_decoded['labels'][0]['base64']);

            // Create and write the Label PDF
            $pdf = fopen (_PS_MODULE_DIR_.'labelgenerator/upload/label_' . $order->reference . '.pdf','w');
            fwrite($pdf,$pdf_decoded);
            fclose($pdf);

            // Create attachment for mail
            $attach = array();
            $content = file_get_contents(_PS_MODULE_DIR_.'labelgenerator/upload/label_' . $order->reference . '.pdf');
            $attach['content'] = $content;
            $attach['name'] ='label_' . $order->reference;
            $attach['mime'] = 'application/pdf';

            // Send mail with PDF Attachment
            Mail::Send(
                Configuration::get('PS_LANG_DEFAULT'),
                'labelmail',
                'En label er genereret for ' + Configuration::get("PS_SHOP_NAME"),
                null,
                Configuration::get("LABEL_EMAIL"),
                null,
                null,
                Configuration::get("PS_SHOP_NAME"),
                $attach,
                null,
                dirname(__FILE__).'/mails/',
                true,
                null,
                null,
                null
            );

            return true;
        }
    }

    // Create Shipment Factory Function
    public function createShipment(Order $order, Customer $customer, Address $address)
    {
        $weight_value = 0;

        // Check if a weight has been added
        if ($order->getTotalWeight() <= 0) {
            $weight_value = 1000;
        } else {
            $weight_value = $order->getTotalWeight();
        }

        // Check if mobile phone data has been added
        if (!$address->mobile_phone) {
            // Otherwise use the address phone
            $address->mobile_phone = $address->phone;
        }

        // Shipment Data Array
        $shipment = [
            "test_mode" => false, 
            "own_agreement" => false, 
            "label_format" => "a4_pdf", 
            "product_code" => "GLSDK_SD", 
            "service_codes" => "EMAIL_NT,SMS_NT", 
            "automatic_select_service_point" => true, 
            "sender" => [
                "name" => Configuration::get("LABEL_NAME"),
                "attention" => null,
                "address1" => Configuration::get("LABEL_ADDRESS_1"),
                "address2" => Configuration::get("LABEL_ADDRESS_2"),
                "zipcode" => Configuration::get("LABEL_ZIPCODE"),
                "city" => Configuration::get("LABEL_CITY"),
                "country_code" => Configuration::get("LABEL_COUNTRY_CODE"),
                "email" => Configuration::get("LABEL_EMAIL"),
                "mobile" => Configuration::get("LABEL_MOBILE"),
                "telephone" => Configuration::get("LABEL_PHONE"),
            ],
            "receiver" => [
                "name" => $address->firstname . ' ' . $address->lastname,
                "attention" => "",
                "address1" => $address->address1,
                "address2" => $address->address2,
                "zipcode" => $address->postcode,
                "city" => $address->city,
                "country_code" => "DK",
                "email" => $customer->email,
                "mobile" => $address->mobile_phone,
                "telephone" => $address->phone,
                "instruction" => $address->other,
            ],
            "parcels" => [
                ["weight" => $weight_value],
            ],
            "print" => false,
            "print_at" => [
                "host_name" => "",
                "printer_name" => "",
                "label_format" => "",
            ],
            "replace_http_status_code" => false,
            "reference" => $order->reference,
        ];

        return $shipment;
    }

    // Configuration Form Validation
    public function getContent()
    {
        $output = null;
    
        if (Tools::isSubmit('submit'.$this->name)) {
            $name = Tools::getValue('LABEL_NAME');
            $address1 = Tools::getValue('LABEL_ADDRESS_1');
            $address2 = Tools::getValue('LABEL_ADDRESS_2');
            $zipcode = Tools::getValue('LABEL_ZIPCODE');
            $city = Tools::getValue('LABEL_CITY');
            $country_code = Tools::getValue('LABEL_COUNTRY_CODE');
            $email = Tools::getValue('LABEL_EMAIL');
            $mobile = Tools::getValue('LABEL_MOBILE');
            $phone = Tools::getValue('LABEL_PHONE');
            $apikey = Tools::getValue('LABEL_APIKEY');
    
            if (
                !$apikey ||
                empty($apikey)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('LABEL_NAME', $name);
                Configuration::updateValue('LABEL_ADDRESS_1', $address1);
                Configuration::updateValue('LABEL_ADDRESS_2', $address2);
                Configuration::updateValue('LABEL_ZIPCODE', $zipcode);
                Configuration::updateValue('LABEL_CITY', $city);
                Configuration::updateValue('LABEL_COUNTRY_CODE', $country_code);
                Configuration::updateValue('LABEL_EMAIL', $email);
                Configuration::updateValue('LABEL_MOBILE', $mobile);
                Configuration::updateValue('LABEL_PHONE', $phone);
                Configuration::updateValue('LABEL_APIKEY', $apikey);

                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
    
        return $output.$this->displayForm();
    }

    // Configuration Form Display Generator
    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');
    
        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->l('Name of shop'),
                    'name' => 'LABEL_NAME',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Address 1'),
                    'name' => 'LABEL_ADDRESS_1',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Address 2'),
                    'name' => 'LABEL_ADDRESS_2',
                    'size' => 20,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Zipcode'),
                    'name' => 'LABEL_ZIPCODE',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('City'),
                    'name' => 'LABEL_CITY',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Country Code (ex: DK)'),
                    'name' => 'LABEL_COUNTRY_CODE',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Email (Senders)'),
                    'name' => 'LABEL_EMAIL',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Mobile'),
                    'name' => 'LABEL_MOBILE',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Phone'),
                    'name' => 'LABEL_PHONE',
                    'size' => 20,
                    'required' => false
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('API Key'),
                    'name' => 'LABEL_APIKEY',
                    'size' => 40,
                    'required' => true
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];
    
        $helper = new HelperForm();
    
        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    
        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;
    
        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];
    
        // Load current value
        $helper->fields_value['LABEL_GENERATOR'] = Configuration::get('LABEL_GENERATOR');
        $helper->fields_value['LABEL_NAME'] = Configuration::get('LABEL_NAME');
        $helper->fields_value['LABEL_ADDRESS_1'] = Configuration::get('LABEL_ADDRESS_1');
        $helper->fields_value['LABEL_ADDRESS_2']= Configuration::get('LABEL_ADDRESS_2');
        $helper->fields_value['LABEL_ZIPCODE'] = Configuration::get('LABEL_ZIPCODE');
        $helper->fields_value['LABEL_CITY'] = Configuration::get('LABEL_CITY');
        $helper->fields_value['LABEL_COUNTRY_CODE'] = Configuration::get('LABEL_COUNTRY_CODE');
        $helper->fields_value['LABEL_EMAIL'] = Configuration::get('LABEL_EMAIL');
        $helper->fields_value['LABEL_MOBILE'] = Configuration::get('LABEL_MOBILE');
        $helper->fields_value['LABEL_PHONE'] = Configuration::get('LABEL_PHONE');
        $helper->fields_value['LABEL_APIKEY'] = Configuration::get('LABEL_APIKEY');
    
        return $helper->generateForm($fieldsForm);
    }
}
